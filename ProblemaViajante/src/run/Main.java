package run;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Matrix;
import model.SolutionGenerator;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		int option = 0;
		int seed = 0;
		int ciudades = 10;
		SolutionGenerator solutionGenerator = null;;
		String ruta = "";
		do {
			System.out.println("Seleccione una opci�n de ejecuci�n\n\t1. Generaci�n de n�meros aleatorios\n\t2. Carga desde fichero");
			option = scanner.nextInt();
			if (option != 1 && option != 2) {
				System.out.println("Opci�n incorrecta");
			}
		} while (option != 1 && option != 2);
		
		if (option == 1) {
			System.out.println("Introduzca una semilla: ");
			seed = scanner.nextInt();
			solutionGenerator = new SolutionGenerator(seed, ciudades - 1);
		} else if (option == 2) {
			System.out.println("Introduzca la ruta del archivo: ");
			scanner = new Scanner(System.in);
			ruta = scanner.nextLine();
			File file = new File(ruta);
			solutionGenerator = new SolutionGenerator(ruta, ciudades - 1);
			
		}
		List<Integer> solucionActual;
		List<Integer> nuevaSolucion;
		Matrix distancias = new Matrix(ciudades - 1);
		Matrix ciudadesVisitadas = new Matrix();
		ciudadesVisitadas.inicializar(ciudades - 2);
		solucionActual = solutionGenerator.getSolucion(option);
		int distanciaTotal = distancias.distanciaTotal(solucionActual);
		System.out.println("**********************SOLUCION S_0**********************\n"
				+ "Recorrido: " + solucionActual + 
				"Distancia: " + distanciaTotal);

		List<Integer> intercambio = new ArrayList<>();
		int numSolucion = 1;
		int numVecino = 0;
		do {
			intercambio = solutionGenerator.generarIntercambio(option);
			if (intercambio != null) {
				if (ciudadesVisitadas.getDato(intercambio.get(0), intercambio.get(1)) == 1) {
					intercambio = ciudadesVisitadas.siguienteIntercambio(intercambio.get(0), intercambio.get(1));
				} else {
					int max = Integer.max(intercambio.get(0), intercambio.get(1));
					int min = Integer.min(intercambio.get(0), intercambio.get(1));
					ciudadesVisitadas.intercambiar(max, min);
				}
				int nuevaDistancia;
				if (intercambio != null) {
					int ciudad0 = solucionActual.get(intercambio.get(0));
					int ciudad1 = solucionActual.get(intercambio.get(1));
					nuevaSolucion = new ArrayList<>(solucionActual);
					nuevaSolucion.set(intercambio.get(0), ciudad1);
					nuevaSolucion.set(intercambio.get(1), ciudad0);
					nuevaDistancia = distancias.distanciaTotal(nuevaSolucion);
					System.out.println("\tVecino V_" + numVecino + 
							"\t�ndices de intercambio: " + intercambio + 
							"\tRecorrido: " + nuevaSolucion + 
							"\tDistancia: " + nuevaDistancia);
					numVecino ++;
					if (nuevaDistancia < distanciaTotal) {
						System.out.println("**********************SOLUCION S_" + numSolucion + "**********************\n"
								+ "Recorrido: " + nuevaSolucion + 
								"\tDistancia: " + nuevaDistancia);
						numVecino = 0;
						solucionActual = nuevaSolucion;
						distanciaTotal = nuevaDistancia;
						ciudadesVisitadas.inicializar(ciudades - 2);
						numSolucion++;
					}

				}
			}
		} while (intercambio != null); // mientras queden ciudades sin visitar
		System.out.println("---------------SOLUCI�N---------------");
		for (int i : solucionActual) {
			System.out.print(i + " ");
		}
		System.out.println("\nDistancia: " + distanciaTotal + " km");
	}

}
