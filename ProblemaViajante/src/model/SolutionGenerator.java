package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SolutionGenerator {
	private int seed;
	private int tamaņoSolucion;
	private List<Integer> solucion;
	private int distancia;
	private Scanner scanner;

	public SolutionGenerator(int seed, int tamaņoSolucion) {
		this.seed = seed;
		this.tamaņoSolucion = tamaņoSolucion;
		solucion = new ArrayList<>();
	}
	
	public SolutionGenerator(String ruta, int tamaņoSolucion) {
		this.tamaņoSolucion = tamaņoSolucion;
		solucion = new ArrayList<>();
		File file = new File(ruta);
		try {
			System.out.println("Abriendo fichero..");
			scanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public int getSeed() {
		return seed;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}
	
	public int getTamaņoSolucion() {
		return tamaņoSolucion;
	}
	
	public void setTamaņoSolucion(int tamaņoSolucion) {
		this.tamaņoSolucion = tamaņoSolucion;
	}
	public List<Integer> getSolucion(int option) {
		generarSolucion(option);
		return solucion;
	}
	
	public void setDistancia(int distancia) {
		this.distancia = distancia;
	}
	
	public int getDistancia() {
		return distancia;
	}
	
	private void generarSolucion(int option) {
		Double num = 0.0;
		int numInt;
		Random random;
		while(solucion.size() < tamaņoSolucion) {
			if (option == 2) {
				if (scanner.hasNext()) {
					num = Double.parseDouble(scanner.nextLine());
				} else {
					System.out.println("Se ha alcanzado el final del fichero");
					System.exit(0);
				}
			} else if (option == 1) {
				random = new Random(seed);
				num = random.nextDouble();
			}
			
			num = Math.floor(num*tamaņoSolucion);
			numInt = num.intValue() + 1;
			while (solucion.contains(numInt)) {
				numInt = (numInt % tamaņoSolucion) + 1;
				if (numInt == 0) {
					numInt = 1;
				}
			}
			solucion.add(numInt);
		}
	}
	
	public List<Integer> generarIntercambio(int option) {
		List <Integer> intercambio = new ArrayList<>();
		Double num = 0.0;
		Random random = null;
		if (option == 2) {
			if (scanner.hasNext()) {
				num = Double.parseDouble(scanner.nextLine());
			} else {
				return null;
			}
		} else if (option == 1) {
			random = new Random(seed);
			num = random.nextDouble();
		}
		
		
		int a = (int) Math.floor(num*tamaņoSolucion);
		
		if (option == 2) {
			if (scanner.hasNext()) {
				num = Double.parseDouble(scanner.nextLine());
			} else {
				return null;
			}
		} else if (option == 1) {
			num = random.nextDouble();
		}
		
		int b = (int)Math.floor(num*tamaņoSolucion);
		
		int max=Math.max(a, b);
		int min=Math.min(a, b);
		if (max==min) {
			min=0;				
			max++;
			if(max>tamaņoSolucion-1)
				max=1;
				

		}
		intercambio.add(max);
		intercambio.add(min);
		return intercambio;
	}
}
