package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Matrix {
	private ArrayList<ArrayList<Integer>> datos;
	private int filas;
	
	public Matrix() {
		datos = new ArrayList<>();
	}
	
	public Matrix(int filas) {
		datos = new ArrayList<>();
		inicializar(filas);
		this.filas = filas;
		int ciudadesFila = 1;
		File file = new File("./input/data.txt");
		try {
			Scanner scanner = new Scanner(file);
			for (int i = 0; i < this.filas; i++) {
				datos.add(i, new ArrayList<>());
				for (int j = 0; j < ciudadesFila; j++) {
					if (scanner.hasNext()) {
						this.setDato(i, j, Integer.parseInt(scanner.next()));
					}
				}
				ciudadesFila++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void inicializar (int filas) {
		datos = new ArrayList<>();
		this.filas = filas;
		int ciudadesFila = 1;
		for (int i = 0; i < this.filas; i++) {
			datos.add(i, new ArrayList<>());
			for (int j = 0; j < ciudadesFila; j++) {
				this.setDato(i, j, 0);
			}
			ciudadesFila++;
		}
	}
	
	public ArrayList<ArrayList<Integer>> getDatos() {
		return datos;
	}
	
	public void setDatos(ArrayList<ArrayList<Integer>> datos) {
		this.datos = datos;
	}
	
	public void setDato (int ciudad1, int ciudad2, int distancia) {
		int max = Integer.max(ciudad1, ciudad2);
		int min = Integer.min(ciudad1, ciudad2);
		datos.get(max).add(min, distancia);
	}
	
	public int getDato (int ciudad1, int ciudad2) {
		int max = Integer.max(ciudad1, ciudad2)-1;
		int min = Integer.min(ciudad1, ciudad2);
		int distancia = datos.get(max).get(min);
		return distancia;
	}
	
	public int distanciaTotal (List<Integer> ciudades) {
		int distancia = 0;
		for (int i = 0; i < ciudades.size()-1; i++) {
			distancia += getDato(ciudades.get(i), ciudades.get(i+1));
		}
		distancia+= getDato(0, ciudades.get(0));
		distancia+= getDato(0, ciudades.get(filas - 1));
		return distancia;
	}
	
	public void intercambiar (int max, int min) {
		datos.get(max - 1).set(min, 1);
	}
	
	public List<Integer> siguienteIntercambio(int ciudad1, int ciudad2) {
		int max = Integer.max(ciudad1, ciudad2);
		int min = Integer.min(ciudad1, ciudad2);
		int tama�o = filas*(filas+1)/2;
		int contador = 1;
		do {
			min ++;
			if (min >= max) {
				min = 0;
				max ++;
				if (max > filas) {
					max = 1;
				}
			} 
			contador ++;
		} while (contador < tama�o && getDato(max, min) == 1);
		if (contador >= tama�o) {
			return null;
		} else {
			this.intercambiar(max, min);
			return Arrays.asList(max, min);	
		}
	}		
}
