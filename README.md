# README #

Implementación en Java de diversos algoritmos de resolución del Problema del Viajante, desarrollado en el marco de la asignatura de Ingeniería del Conocimiento de 4º de Grado en Ingeniería Informática.

**Implementaciones**

* Búsqueda local (El primer mejor)
* Búsqueda tabú
* Temple simulado
* Computación evolutiva

Para la implementación de cada uno de los algoritmos, se parte de un pseudocódigo proporcionado por los profesores de la asignatura de Ingeniería del Conocimiento, sobre el que se han realizado una serie de optimizaciones.